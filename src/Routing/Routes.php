<?php declare(strict_types=1);

namespace Drupal\ecwid\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\Routing\Route;

class Routes implements ContainerInjectionInterface {
  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get("config.factory"),
      $container->get('entity_type.manager')
    );
  }

  public function routes(): array {
    $base_path = $this->configFactory
      ->get("ecwid.settings")
      ->get("store_base_path");

    $routes = [];

    if ($base_path) {
      $routes["ecwid.base_path"] = new Route(
        $base_path,
        [
          "_controller" =>
            '\Drupal\ecwid\Controller\EcwidPageController::handleStoreRequest',
        ],
        // ["_permission" => "view ecwid_page entities"]
        ["_permission" => "access content"]
      );
      $routes["ecwid.category_internal"] = new Route(
        $base_path . "/{categoryPath}",
        [
          "_controller" =>
            '\Drupal\ecwid\Controller\EcwidPageController::handleCategoryRequest',
        ],
        [
          "categoryPath" => ".+-c[\d]+$",
          // "_permission" => "view ecwid_page entities"
          "_permission" => "access content",
        ]
      );
      // $categoryRoute = new Route(
      //   $base_path . "/{slug}-c{categoryId}",
      //   [
      //     "_controller" =>
      //       '\Drupal\ecwid\Controller\EcwidPageController::handleCategory',
      //     "_title" => "Category",
      //   ],
      //   [
      //     "slug" => "[^-]+",
      //     "categoryId" => "\d+",
      //   ],
      //   ["utf8" => true]
      // );
      // $collection->add("ecwid.category", $categoryRoute);
    }

    return $routes;
  }
}
