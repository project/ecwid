<?php

namespace Drupal\Tests\ecwid\Functional;

use Drupal\Tests\BrowserTestBase;

class EcwidAdminTest extends BrowserTestBase
{
    /**
     * {@inheritdoc}
     */
    protected static $modules = ['ecwid'];

    /**
     * {@inheritdoc}
     */
    protected $defaultTheme = 'stark';

    public function testEcwidAdminPage()
    {
        // Create and log in an administrative user with permission.
        $admin_user = $this->drupalCreateUser(['administer site configuration']);
        $this->drupalLogin($admin_user);
        // Go to the Ecwid admin settings page and check the response.
        $this->drupalGet('/admin/config/services/ecwid');
        $this->assertSession()->statusCodeEquals(200);
        $this->assertSession()->pageTextContains('Connect your Ecwid store');
    }

    protected function setUp() : void
    {
        parent::setUp();
        // Create and log in an administrative user with permission.
        $this->adminUser = $this->drupalCreateUser(['administer site configuration', 'access administration pages']);
        $this->drupalLogin($this->adminUser);
    }
}