<?php declare(strict_types=1);

namespace Drupal\ecwid;

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;

enum UrlType: string
{
  case Product = "product";
  case Category = "category";
  case ProductAndCategory = "productAndCategory";
}

class UrlInvalidException extends \Exception
{
}

class UrlParser
{
  /**
   * The Ecwid URL, may be a category or a product.
   */
  protected string $url;

  /**
   * Internal copy of the mode from the URL.
   *
   * Should be 'p' | 'c'
   */
  private string $type;

  /**
   * Internal copy of the id from the URL.
   */
  private string $id;

  /**
   * Ensures that the provided URL matches the expected Ecwid URL pattern.
   *
   * This method checks if the URL stored in the class instance matches
   * a specific pattern indicative of Ecwid URLs. It uses a regular expression
   * to check for this pattern. If the URL does not match the pattern,
   * an exception is thrown.
   *
   * Parses URL components into private class properties, but leaves making
   * judgements about output types to public methods.
   *
   * @throws \Drupal\ecwid\UrlInvalidException
   *   Thrown when the URL does not match the Ecwid URL pattern. The exception
   *   message includes the sanitized version of the invalid URL for safe display.
   */
  private function ensureEcwidUrl()
  {
    $pattern = "!.*-(p|c)([0-9]+)!";
    $matches = [];
    $matchSuccessful = preg_match($pattern, $this->url, $matches);
    $modeIsValid = in_array($matches[0], ["p", "c"]);
    $idIsValid = is_numeric($matches[1]);

    if (!($matchSuccessful && $modeIsValid && $idIsValid)) {
      $safeUrl = Html::escape($this->url);
      throw new UrlInvalidException(
        "The URL {$safeUrl} is not a valid Ecwid URL."
      );
    }

    $this->type = $matches[0];
    $this->id = $matches[1];
  }

  /**
   * Construct an Ecwid store URL.
   *
   * @throws \Drupal\ecwid\UrlInvalidException
   *   Thrown when the URL does not match the Ecwid URL pattern. The exception
   *   message includes the sanitized version of the invalid URL for safe display.
   */
  public function __construct(string $url)
  {
    $this->url = $url;
    $this->ensureEcwidUrl();
  }

  /**
   * Determines if the URL is a product or category.
   *
   * @throws \Exception
   *  Thrown when the URL is something other than 'c' or 'p'.
   */
  public function getType(): UrlType
  {
    return match ($this->type) {
      "c" => UrlType::Category,
      "p" => UrlType::Product,
      "pc" => UrlType::ProductAndCategory,
    };
  }

  public function getId(): string
  {
    return $this->id;
  }

  public function asUrl(): Url
  {
    // @todo fill this in.
    return Url::fromRoute("/");
  }
}
