<?php declare(strict_types=1);

namespace Drupal\ecwid\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\ecwid\EcwidApiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfigForm extends ConfigFormBase
{
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return ["ecwid.settings"];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return "ecwid_settings_form";
  }

  /**
   * The Ecwid API service.
   *
   * @var \Drupal\ecwid\EcwidApiService
   *   The Ecwid API service.
   */
  protected EcwidApiService $ecwidApiService;

  /**
   * Constructs a new ConfigForm.
   *
   * @param \Drupal\ecwid\EcwidApiService $ecwidApiService
   *   The Ecwid API service.
   */
  public function __construct(EcwidApiService $ecwidApiService)
  {
    $this->ecwidApiService = $ecwidApiService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static($container->get("ecwid.ecwid_api_service"));
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config("ecwid.settings");
    $form = parent::buildForm($form, $form_state);

    // #title here matches what is in the tests.
    $form["connect_store"] = [
      "#type" => "details",
      "#title" => $this->t("Connect your Ecwid store"),
      "#open" => true,
    ];

    $form["connect_store"]["store_id"] = [
      "#type" => "textfield",
      "#title" => $this->t("Store ID"),
      "#description" => $this->t("Enter your Ecwid store ID."),
      "#required" => true,
      "#maxlength" => 255,
      "#default_value" => $config->get("store_id"),
    ];

    $form["connect_store"]["connect_store"]["validate_store_id"] = [
      "#type" => "button",
      "#value" => $this->t("Validate Store ID"),
      "#ajax" => [
        "callback" => "::validateStoreIdAjax",
        "wrapper" => "store-id-validate-result",
      ],
    ];

    $form["connect_store"]["connect_store"]["validation_result"] = [
      "#type" => "markup",
      "#markup" => '<div id="store-id-validate-result"></div>',
    ];

    // Advanced settings fieldset
    $form["advanced"] = [
      "#type" => "details",
      "#title" => $this->t("Advanced"),
      "#open" => false, // Collapsed by default
    ];

    // Ecwid store base path field, e.g. '/products'
    $form["advanced"]["store_base_path"] = [
      "#type" => "textfield", // URL field type isn't available, using textfield
      "#title" => $this->t("Ecwid store base path"),
      "#description" => $this->t(
        'Every Ecwid URL will start with this. For example, in Ecwid there might be a product at the URL /foo-category/bar-product-p1234, setting the base URL to "/product" will make that product appear at /product/foo-category/bar-product-p1234.'
      ),
      "#default_value" => $config->get("store_base_path") ?: "/products",
      "#required" => true,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);
    $store_id = $form_state->getValue("store_id");

    if ($this->ecwidApiService->validateEcwidStore($store_id)) {
      // Store the configuration if the Ecwid store is validated.
      $config = $this->config("ecwid.settings");
      $config->set("store_id", $store_id)->save();

      // Save the Ecwid store base URL
      $store_base_path = $form_state->getValue("store_base_path");
      $config->set("store_base_path", $store_base_path)->save();
    } else {
      // Display an error if the store cannot be validated.
      \Drupal::messenger()->addError(
        $this->t("Invalid store ID or store cannot be reached at this moment.")
      );
    }
  }

  public function validateStoreIdAjax(
    array &$form,
    FormStateInterface $form_state
  ) {
    $ajax_response = new AjaxResponse();
    $store_id = $form_state->getValue("store_id");

    // Validate the store ID by checking with Ecwid if store exists.
    if ($this->ecwidApiService->validateEcwidStore($store_id)) {
      $profile = $this->ecwidApiService->getStoreProfile();

      $text = $this->t("Successfully connected to Ecwid store: @store_id", [
        "@store_id" => $profile["storeName"],
      ]);

      $ajax_response->addCommand(
        new HtmlCommand("#store-id-validate-result", $text)
      );
    } else {
      $text = $this->t(
        "Failed to connect to Ecwid store. Please check the store ID."
      );

      $ajax_response->addCommand(
        new HtmlCommand("#store-id-validate-result", $text)
      );
    }

    return $ajax_response;
  }
}
