<?php declare(strict_types=1);

namespace Drupal\ecwid\Exception;

/**
 * Represents a request to an invalid or malformed Ecwid API path.
 */
class InvalidPathException extends EcwidApiError
{
}
