<?php declare(strict_types=1);

namespace Drupal\ecwid\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\ecwid\EcwidApiService;
use Drupal\ecwid\Entity\EcwidPage;
use Drupal\ecwid\Exception\TypeError;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EcwidPageController extends ControllerBase
{
  public function view(EcwidPage $ecwid_page, Request $request)
  {
    $storeId = $this->ensureStoreId()
      ->config("ecwid.settings")
      ->get("store_id");

    $category = $this->ecwidApiService->getCategory(
      is_numeric($ecwid_page->ecwid_category->value)
        ? (int) $ecwid_page->ecwid_category->value
        : throw new TypeError("Ecwid category of " . $ecwid_page->ecwid_category->value . " is not an integer")
    );

    return [
      "#theme" => "ecwid_store_block",
      "#store_id" => $storeId,
      "#default_category" => $category,
      "#base_path" => $request->getPathInfo(),
    ];
  }

  /**
   * Request for the store front, no category or product selected.
   */
  public function handleStoreRequest(Request $request)
  {
    $storeId = $this->ensureStoreId()
      ->config("ecwid.settings")
      ->get("store_id");

    return [
      "#theme" => "ecwid_store_block",
      "#store_id" => $storeId,
      "#base_path" => $request->getPathInfo(),
    ];
  }

  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get("ecwid.ecwid_api_service"),
      $container->get("config.factory"),
      $container->get("logger.factory")
    );
  }

  public function __construct(
    protected readonly EcwidApiService $ecwidApiService,
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactoryInterface $loggerFactory
  ) {
    $this->configFactory = $configFactory;
    $this->loggerFactory = $loggerFactory;
  }

  public function ensureStoreId(): static
  {
    if (!$this->ecwidApiService->validateEcwidStore()) {
      // If the user has permission to setup the store, give them a helpful
      // link in the event their store id has not been set.
      if ($this->currentUser()->hasPermission("administer ecwid")) {
        $link = Link::createFromRoute(
          $this->t("Ecwid settings"),
          "ecwid.settings_form"
        )->toString();

        $this->getLogger("ecwid")->critical(
          $this->t(
            "The Ecwid store is invalid. Please set or update your store id: @link.",
            [
              "@link" => $link,
            ]
          )
        );
      }

      // Throw a 404 when the store id is not set.
      throw new NotFoundHttpException();
    }

    return $this;
  }

  public function handleCategoryRequest(
    array $ecwid_category,
    Request $request
  ): array {
    $storeId = $this->ensureStoreId()
      ->config("ecwid.settings")
      ->get("store_id");

    if (strpos($request->getPathInfo(), "/ecwid-store") === 0) {
      $this->loggerFactory
        ->get("ecwid")
        ->error(
          "Direct request to ecwid-store URI detected! These routes are for internal use only." .
            PHP_EOL .
            print_r($request, true)
        );
    }

    return [
      "#theme" => "ecwid_page",
      "#store_id" => $storeId,
      '#store_base_path' => $this->config("ecwid.settings")->get("store_base_path"),
      "#default_category" => $ecwid_category,
      "#base_path" => $request->getPathInfo(),
    ];
  }

  public function handleProductRequest(
    array $ecwid_product,
    array|null $ecwid_category,
    Request $request
  ): array {
    $storeId = $this->ensureStoreId()
      ->config("ecwid.settings")
      ->get("store_id");

    if (strpos($request->getPathInfo(), "/ecwid-store") === 0) {
      $this->loggerFactory
        ->get("ecwid")
        ->error(
          "Direct request to ecwid-store URI detected! These routes are for internal use only." .
            PHP_EOL .
            print_r($request, true)
        );
    }

    // Get all but the product to use as the base_url.
    // @todo: This is a bit hacky, replace with something more formal once
    // an Ecwid Url class and parser are written.
    $basePath = implode(
      "/",
      array_slice(explode("/", $request->getPathInfo()), 0, -1)
    );

    $return = [
      "#theme" => "ecwid_page",
      "#store_id" => $storeId,
      "#default_product" => $ecwid_product,
      "#base_path" => $basePath,
      ...empty($ecwid_category) ? [] : ["#default_category" => $ecwid_category],
    ];
    return $return;
  }

  /**
   * Respond to requests for internal Ecwid URLs.
   *
   * For example:
   *  /product/some-category/SOME_ECWID_GENERATED_URL-p12345
   *   ^store base path ^Drupal path ^Ecwid path with product number.
   *
   * @see Drupal\ecwid\PathProcessor\EcwidPathProcessor
   * @see Drupal\ecwid\Routing\RouteSubscriber
   */
  public function handleProduct(string $slug, string|int $productId): array
  {
    $config = $this->config("ecwid.settings");
    $storeId = $config->get("store_id");
    $basePath = $config->get("store_base_path");

    return [
      "#theme" => "ecwid_store_block",
      "#store_id" => $storeId,
      "#base_path" => $basePath,
    ];
  }
}
