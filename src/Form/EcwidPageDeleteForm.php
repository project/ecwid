<?php declare(strict_types=1);

namespace Drupal\ecwid\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
/**
 * Provides a form for deleting Ecwid page entities.
 *
 * @ingroup ecwid
 */
class EcwidPageDeleteForm extends EntityConfirmFormBase
{
  /**
   * {@inheritdoc}
   */
  public function getQuestion()
  {
    return $this->t("Are you sure you want to delete entity %name?", [
      "%name" => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl()
  {
    return new Url("entity.ecwid_page.collection");
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription()
  {
    return $this->t("Deleting this entity cannot be undone.");
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    // Get the ID of the deleted entity.
    $entity_id = $this->entity->id();

    // Load the corresponding menu links.
    $menu_link_manager = \Drupal::service("plugin.manager.menu.link");
    $menu_links = $menu_link_manager->loadLinksByRoute(
      "entity:ecwid_page/" . $entity_id
    );

    // Delete each menu link associated with the entity.
    foreach ($menu_links as $menu_link) {
      $menu_link->delete();
    }
    $this->entity->delete();

    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}
